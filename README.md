# svelte-component-scaffold

## Prototype links
[Invision prototype design](https://projects.invisionapp.com/)


## Setup and run
Rename src/NewComponent.html
Update src/Container.html to ref your new component name

```bash
npm i
npm run dev
```
Open:
[localhost:3000](http://localhost:3000)


## Assets
To server your static files from the dev server add your files to the[server/assets](server/assets) directory.